import React from 'react';

const Listado = ({factura, eliminarFactura, descargarFactura}) => {
    
    {factura.precioFinal = parseInt(factura.precio) * parseInt(factura.cantidad)}
    
    return ( 
        <div className="factura">
            <p>Nombre: <span>{factura.nombre}</span></p>
            <p>Apellido: <span>{factura.apellido}</span></p>
            <p>Descripcion: <span>{factura.descripcion} x {factura.cantidad}U.</span></p>
            <p>Precio Final: $<span>{parseInt(factura.precioFinal)}</span></p>
        
            <button
                className="button eliminar u-full-width"
                onClick={ () => eliminarFactura(factura.id)}
            >Eliminar &times;</button>

            <button
                className="button descargar u-full-width"
                onClick={ () => descargarFactura(factura.id)}
            >Descargar</button>
        </div>
     );
}
 
export default Listado;
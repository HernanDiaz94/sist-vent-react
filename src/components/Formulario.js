import React, { Fragment, useState} from 'react';
//import uuid from 'uuid/v4';
import shortid from 'shortid';

const Formulario = ({crearFactura}) => {
    
    const [factura, actualizarFactura] = useState({
        nombre: '',
        apellido: '',
        precio:'',
        cantidad:'',
        descripcion:'',
        precioFinal:0,
    });

    const [error, actualizarError] = useState(false);

    //funcion para saber si se actualiza el state
    const actualizarState = e => {
        actualizarFactura({
            ...factura,
            [e.target.name]: e.target.value
        })
    }
    
    //extraer los valores
    const {nombre, apellido,precio, cantidad, descripcion} = factura;


    //Cuando presiona enviar datos
    const submitFactura = e => {
        e.preventDefault();

        //Validar
        if(nombre.trim() === '' || apellido.trim()===''){
            actualizarError(true);
            return;
        }
        //elimina cartel de error
        actualizarError(false);

        //Asignar ID
        //factura.id = uuid();
        factura.id = shortid.generate();
        
        //Crear factura
        crearFactura(factura);

        //Reiniciar form
        actualizarFactura({
            nombre: '',
            apellido: '',
            precio:'',
            cantidad:'',
            descripcion:'',
        })

    }


    return ( 
        <Fragment>
            <h2>Formulario Venta</h2>

            {error
            ?<p className="alerta-error">
             Todos los campos son obligatorios</p>
            :null
            }

            <form
                onSubmit={submitFactura}
            >
                <label>Nombre/s</label>
                <input 
                    type="text"
                    name="nombre"
                    className="u-full-width"
                    placeholder="Ingrese Nombre/s"
                    onChange={actualizarState}
                    value={nombre}
                />

                <label>Apellido/s</label>
                <input 
                    type="text"
                    name="apellido"
                    className="u-full-width"
                    placeholder="Ingrese Apellido/s"
                    onChange={actualizarState}
                    value={apellido}
                />
                
                <label>Precio</label>
                <input 
                    type="number"
                    name="precio"
                    className="u-full-width"
                    placeholder="Precio $"
                    onChange={actualizarState}
                    value={precio}
                />

                <label>Cantidad</label>
                <input 
                    type="number"
                    name="cantidad"
                    className="u-full-width"
                    placeholder="Cantidad"
                    onChange={actualizarState}
                    value={cantidad}
                />

                <label>Descripcion</label>
                <textarea
                    name="descripcion"
                    className="u-full-width"
                    placeholder="Descripcion producto"
                    onChange={actualizarState}
                    value={descripcion}
                ></textarea>
                

                <button
                    type="submit"
                    className="u-full-width button-primary"
                >Enviar Datos</button>
            </form>
        </Fragment>
        
     );
}
 
export default Formulario;
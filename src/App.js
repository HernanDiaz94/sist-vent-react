import React,{Fragment, useState} from 'react';
import Formulario from './components/Formulario';
import Listado from './components/Listado';

function App() {

  //Array de facturas
  const [facturas, guardarFacturas] = useState([]);

  //Funcion que tome la factura actual y agrega una nueva
  const crearFactura = factura => {
    guardarFacturas([
      ...facturas,
         factura
    ]);
    //console.log(factura);
  }

  const eliminarFactura = id => {
    //console.log(id);
    const nuevasFacturas = facturas.filter( factura => factura.id !== id);
    guardarFacturas(nuevasFacturas);
  }

  //mensaje de que no hay facturas
  const tituloFacturas = facturas.length === 0 
  ? 'No hay Facturas'
  : 'Factura/s';

  const descargarFactura = id => {
    //el boton descarga se conecta
    console.log(id);
  }

  return (
    <Fragment>
        <h1>Facturas</h1>

        <div className="container">
          <div className="row">
            <div className="one-half column">
                <Formulario
                  crearFactura={crearFactura}
                />
            </div>

            <div className="one-half column">
                <h2>{tituloFacturas}</h2>
                {facturas.map(factura => (
                  <Listado 
                    key={factura.id}
                    factura={factura}
                    eliminarFactura={eliminarFactura}
                    descargarFactura={descargarFactura}
                  />
                ))}
            </div>
          </div>

        </div>
    </Fragment>
    
    
  );
}

export default App;
